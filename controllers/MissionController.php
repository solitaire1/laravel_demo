<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Illuminate\Http\Response;
use Illuminate\Support\Facades\Response;
use App\User;
use App\PushNotify;
use Hash;
use Mail;
use Session;
use Auth;
use App\Common;

class MissionController extends Controller {

    protected $user;

    public function __construct(Request $request) {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_type;

            if ($this->user != 1) {
                return redirect()->back();
            }
            return $next($request);
        });
    }

    public function mission() {
        $data['mission'] = Common::getData($table_name = "missions");
        return view('admin.mission.mission', $data);
    }

    public function create_mission() {

        $data['customer'] = User::get_all_user($user_type = 2);
        $data['service_provider'] = User::get_all_user($user_type = 3);
        $data['object_leader'] = User::get_all_user($user_type = 4);
        return view('admin.mission.create_mission', $data);
    }

    public function CreateMission(Request $request) {
        $service_provider = $request->input('service_provider');
        $customer = $request->input('customer');

        if (empty($request->input('service_provider'))) {
            Session::flash('message', 'Please Select Provider !');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back()->withInput($request->all);
        }
        if (empty($request->input('customer'))) {
            Session::flash('message', 'Please Select Customer !');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back()->withInput($request->all);
        }
        if (empty($request->input('object_leader'))) {
            Session::flash('message', 'Please Select Object Leader !');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back()->withInput($request->all);
        }
        if (empty($request->input('detail'))) {
            Session::flash('message', 'Please Insert Detail !');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back()->withInput($request->all);
        }
        if (empty($request->input('address'))) {
            Session::flash('message', 'Please Insert address !');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back()->withInput($request->all);
        }
        $data['service_provider_id'] = $request->input('service_provider');
        $data['customer_id'] = $request->input('customer');
        $data['object_leader_id'] = $request->input('object_leader');
        $data['detail'] = $request->input('detail');
        $data['address'] = $request->input('address');
        $data['unique_code'] = ucfirst($this->generateRandomString());
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['date'] = date("Y-m-d");
        $data['updated_at'] = date("Y-m-d H:i:s");
        $result = Common::InsertMission($data);
        if ($result) {

            $serv_provi_data = User::where('id', $service_provider)->first();
            $customer_data = User::where('id', $customer)->first();
            $notifyMsg = 'New mission added';

            if ($serv_provi_data->device_token != "") {
                PushNotify::send_notif($serv_provi_data->device_token, $notifyMsg, 2, $result, $data['unique_code'], $data['service_provider_id'], $data['object_leader_id'], $data['customer_id']);
            }

            if ($customer_data->device_token != "") {
                PushNotify::send_notif($customer_data->device_token, $notifyMsg, 2, $result, $data['unique_code'], $data['service_provider_id'], $data['object_leader_id'], $data['customer_id']);
            }

            $notificationData = ['customer_id' => $customer_data->id,
                'service_provider_id' => $service_provider,
                'mission_id' => $result,
                'message' => $notifyMsg,
                'type' => '2',
                'created_at' => date('Y-m-d H:i:s')
            ];

            DB::table('notification')->insert($notificationData);

            Session::flash('message', 'Mission added successfully');
            Session::flash('alert-class', 'alert-success');
            return redirect('/missions');
        } else {
            Session::flash('message', 'Something went wrong');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }
    }

    function generateRandomString($length = 5) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function DeleteMission($id) {
        $result = Common::DeleteMission($id);
        if ($result) {
            Session::flash('message', 'Mission Deleted successfully');
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();
            die;
        } else {
            Session::flash('message', 'Something went wrong');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
            die;
        }
    }

    public function AddCustomerInMission(Request $request) {

        if (empty($request->input('name'))) {
            Session::flash('message', 'Please enter client name !');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/create_mission');
        }
        if (!preg_match("/^[a-zA-Z\s]+$/", $request->input('name'))) {
            Session::flash('message', 'Please enter the valid client name !');
            Session::flash('alert-class', 'alert-danger');

            return redirect('/create_mission');
        }
        if (empty($request->input('email'))) {
            Session::flash('message', 'Please enter the email address!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/create_mission');
        }
        if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
            Session::flash('message', 'Please enter the valid email id !');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/create_mission');
        }
        $user_check = User::check_user_by_email($request->input('email'));
        if (count($user_check) > 0) {
            Session::flash('message', 'Email id already exist!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/create_mission');
        }
        if (empty($request->input('phone'))) {
            Session::flash('message', 'Please enter phone number!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/create_mission');
        }
        if (strlen($request->input('phone')) != 10) {
            Session::flash('message', 'Please enter valid phone number!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/create_mission');
        }
        if (preg_match("/[^0-9,-]/", $request->input('phone'))) {
            Session::flash('message', 'Please enter valid phone number !');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/create_mission');
        }

        $data['name'] = $request->input('name');
        $data['email'] = $request->input('email');
        $data['phone'] = $request->input('phone');
        $password = uniqid();
        $data['password'] = md5($password);
        $data['user_type'] = 2;
        $to_emails = array($request->input('email'));
        $strFromEmail = 'solitaireinfosys123@gmail.com';
        $strFromName = 'solitaireinfosys123@gmail.com';
        $viewContent = '<!DOCTYPE html>
                    <html>
                    <head>
                    <title>Email Template</title>
                    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
                    </head>
                    <body>
                    <div style="width:700px; display: block;margin: 10px auto;">
                    <table style="border-collapse: collapse; border:2px solid #ececec;width:100%">
                    <tr style="background: #ececec">                    
                    <td align="center" style="padding: 10px 0;"><img src="http://112.196.33.91/winter_app/logo.png"></td>
                    </tr>
                    <tr>
                    <td style="font-family: "Open Sans", sans-serif; ">
                    <div style=" min-height:400px; width:100%; display: block; box-sizing: border-box;padding: 10px 20px 50px">
                    <h2 style="margin-bottom: 20px"> <b>Customer</b></h2>
                    <p style="margin-bottom: 20px">Email: <b>Hello ' . $request->input('name') . ', please login with following details: </b></p>
                    <p style="margin-bottom: 20px">Email: <b>' . $request->input('email') . '</b></p>
                    <p style="margin-bottom: 20px">Password: <b>' . $password . '</b></p>
                    <p style="margin:0px">Regards,</p>
                    <p style="margin:0px">Maschinenring Winter Services Team</p>
                    </div>
                    </td>
                    </tr>
                    <tr style="background: #ececec">
                    <td align="center" style="color: #1e1e1e;font-family: "Open Sans", sans-serif;padding:10px 0; font-size: 14px;">&copy; 2018, Maschinenring Winter Services App</td>
                    </tr>
                    </table>
                    </body>
                    </html>';
        try {
            $mail = Mail::send([], [], function ($message) use($to_emails, $strFromEmail, $strFromName, $viewContent) {
                        $message
                                ->from($strFromEmail, $strFromName)
                                ->to($to_emails)
                                ->subject('Account registration')
                                ->setBody($viewContent, 'text/html');
                    });
        } catch (Exception $e) {
            Session::flash('message', 'Something went wrong');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }
        $result = DB::table('users')->insert($data);
        if ($result) {
            Session::flash('message', 'Customer added successfully');
            Session::flash('alert-class', 'alert-success');
            return redirect('/create_mission');
            die;
        } else {
            Session::flash('message', 'Something went wrong');
            Session::flash('alert-class', 'alert-danger');
            return redirect('/create_mission');
        }
    }

    public function editMission($id) {
        $data['mission'] = Common::editMission($id);
        $data['dispatchMission'] = Common::disspatchMissionDetail($id);
        $data['finalMissionDetail'] = Common::finalMissionDetail($id);
        //print_r($data['dispatchMission']); die;
        return view('admin.mission.view_mission', $data);
    }

}
