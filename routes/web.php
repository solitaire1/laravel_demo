<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('auth.login');
});


/* ###### Admin Routing client Section ####### */
Route::group(['middleware' => ['auth', 'web']], function () {

################### mission admin section#######################
    Route::get('/missions', 'MissionController@mission');
    Route::get('/create_mission', 'MissionController@create_mission');
    Route::any('/CreateMission', 'MissionController@CreateMission');
    Route::any('/AddCustomerInMission/', 'MissionController@AddCustomerInMission');
    Route::get('/DeleteMission/{id}', 'MissionController@DeleteMission');
    Route::get('/editMission/{id}', 'MissionController@editMission');
#################################################################

});

Auth::routes();


