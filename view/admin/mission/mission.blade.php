@extends('layouts.index')
@section('content')
<?php

use App\Common;
use App\User;
?>
<div class="col-10 col-sm-8 col-md-9 col-lg-10 col-xl-10 col-lg content_side">
    <div class="row">
        <header>
            <div class="col">
                <h2>
                    Missions <a href="{{url('/logout')}}" class="float-right">Log out <img src="{{ app('url')->asset('public/'.'images/icons/icon-logout.png') }}"></a>
                </h2>
            </div>
        </header>
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <section class="data_filter">
            <div class="col">
                <form class="form-inline">
                    <div class="form-group">
                        <input type="search" class="form-control" id="search_data_table" placeholder="Search">
                    </div>
                    <div class="form-group ml-2">
                        <a href="{{url('/create_mission')}}" class="btn btn-green-round">Create a Mission</a>
                    </div>
                </form>
            </div>
        </section>
        <section class="data_section">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-bordered" id="data_table">
                        <thead>
                            <tr>
                                <th scope="col">S.No.</th>
                                <th scope="col">Code</th>
                                <th scope="col">Customer Name</th>
                                <th scope="col">Service Provider</th>
                                <th scope="col">Object Leader</th>
                                <th scope="col">Address</th>
                                <th scope="col">Status</th>
                                <th  scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($mission as $key=>$val)
                            <tr>
                                <td scope="row">{{$key+1}}</td>
                                <td>{{$val->unique_code}}</td>
                                <td>{{User::getUserByid($val->customer_id)}}</td>
                                <td>{{User::getUserByid($val->service_provider_id)}}</td>
                                <td>{{User::getUserByid($val->object_leader_id)}}</td>
                                <td><?php echo substr($val->address, 0, 10) . '...'; ?></td>
                                <td><?php
                                    if ($val->status == 1) {
                                        echo "New";
                                    } elseif ($val->status == 2) {
                                        echo "Accepted";
                                    } elseif ($val->status == 3) {
                                        echo "Rejected";
                                    } elseif ($val->status == 4) {
                                        echo "Pending";
                                    } elseif ($val->status == 5) {
                                        echo "Dispatched";
                                    }elseif ($val->status == 6) {
                                        echo "Completed";
                                    }
                                    ?></td>
                                <td>

                                    <a href="{{url('/editMission')}}/{{$val->id}}"><img src="{{ app('url')->asset('public/'.'images/icons/eye.png') }}"></a>
                                    <a href="{{url('/DeleteMission')}}/{{$val->id}}" onclick="return confirm('Are you sure you want to delete mission?')"><img src="{{ app('url')->asset('public/'.'images/icons/icon-trash@2x.png') }}"></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection