@extends('layouts.index')
@section('content')
<div class="col-10 col-sm-8 col-md-9 col-lg-10 col-xl-10 col-lg content_side">
    <div class="row">
        <header>
            <div class="col">
                <h2>
                    <span>Missions / </span> <a href="javascript:void(0);">Create a Mission</a> <a href="{{url('/logout')}}" class="float-right">Log out <img src="{{ app('url')->asset('public/'.'images/icons/icon-logout.png') }}"></a>
                </h2>
            </div>
        </header>
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <section class="data_section">
            <div class="col">
                <div class="row">
                    <div class="col-12">
                        <div class="form_section">
                            <fieldset>
                                <legend>Mission Details</legend>
                                <div class="row justify-content-md-center" >
                                    <div class="col-12 col-sm-12">
                                        <form class="row" action="{{url('/CreateMission')}}" method="post">
                                            {{ csrf_field() }}
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group row">
                                                    <label class="col-12 col-lg-4 col-form-label">Customer Email</label>
                                                    <div class="col-lg-8 col-12 ">
                                                        <select name="customer" id="basic" data-live-search="true" data-btn-class="btn-error" data-live-search-placeholder="Search" class="form-control selectpicker">
                                                            <option value=""></option>
                                                            @foreach($customer as $key=>$value)
                                                            <option value="{{$value->id}}"> {{$value->email}}</option>
                                                            @endforeach
                                                        </select>
                                                        <a href="javascript:void(0);" class="add-button" data-action='add-customer'><img src="{{ app('url')->asset('public/'.'images/icons/plus.png') }}"> ADD</a>
                                                    </div>
                                                </div>	
                                            </div>
                                           
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Service Provider</label>
                                                    <div class="col-lg-8 col-12">
                                                        <select name="service_provider" data-live-search="true" data-btn-class="btn-error" data-live-search-placeholder="Search" class="form-control selectpicker ">
                                                            <option value=""></option>
                                                            @foreach($service_provider as $key=>$value)
                                                            <option value="{{$value->id}}"> {{$value->email}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                </div>	
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Object Leader</label>
                                                    <div class="col-lg-8 col-12 ">
                                                        <select name="object_leader" data-live-search="true" data-btn-class="btn-error" data-live-search-placeholder="Search" class="form-control  selectpicker">
                                                            <option value=""></option>
                                                            @foreach($object_leader as $key=>$value)
                                                            <option value="{{$value->id}}"> {{$value->email}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Address</label>
                                                    <div class="col-lg ">
                                                        <input type="text" class="form-control" required="" id="formGroupExampleInput" placeholder="Address" name="address">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Mission Details</label>
                                                    <div class="col-lg ">
                                                        <textarea class="form-control" required="" name="detail" id="exampleFormControlTextarea1" placeholder="Mission Details" rows="4"></textarea>
                                                    </div>
                                                </div>	
                                            </div>
                                            <div class="col-12 col-sm-6 text-right">
                                                <div class="display_table">
                                                    <div class="display_table_cell align-bottom">
                                                        <div class="form-group">
                                                            <!-- <button type="submit" class="btn-save btn mr-2">Create</button> -->
                                                            <input type="submit" class="btn-save btn mr-2" name="" value="Create">
                                                            <button class="btn-cancel btn">Cancel</button>
                                                        </div>						
                                                    </div>
                                                </div>
                                            </div>					
                                        </form>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- Add new customer popup -->
<div class="position-fixed d-none" id="add-customer">
    <div class="display_table">
        <div class="display_table_cell text-center">
            <div class="width_500 display_inilin_block text-left ">
                <div class="form_section mb-0">
                    <div class="col-12 pt-0 pb-2 text-right">
                        <a href="javascript:void(0);" class="close-button" data-action='add-customer'><img src="{{ app('url')->asset('public/'.'images/icons/cross.png') }}"></a>
                    </div>
                    <fieldset>
                        <legend>Add New Customer</legend>
                        <div class="row justify-content-md-center">
                            <div class="col-12 col-sm-12">
                                <form class="row" id="add_customer_data1"  action="{{url('/AddCustomerInMission')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label">Customer</label>
                                            <div class="col-lg ">
                                                <div class="input-group">			
                                                    <input type="text" required="required" class="form-control" id="formGroupExampleInput" name="name" placeholder="Customer Name">
                                                </div>
                                            </div>
                                        </div>	
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label on">Phone</label>
                                            <div class="col-lg ">
                                                <div class="input-group">			
                                                    <input type="text" required="required" maxlength="10" minlength="10" class="form-control only-numbers" id="formGroupExampleInput" placeholder="Phone" name="phone">
                                                </div>
                                            </div>	      
                                        </div>	
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label">Email ID</label>
                                            <div class="col-lg ">
                                                <div class="input-group">			
                                                    <input type="email" required="required" class="form-control" id="formGroupExampleInput" name="email" placeholder="Email ID">
                                                </div>
                                            </div>
                                        </div>	
                                    </div>
                                    <div class="col-12 text-center mt-3">
                                        <div class="display_table">
                                            <div class="display_table_cell align-bottom">
                                                <div class="form-group">
                                                    <input type="submit"   class="add-customer btn-save btn mr-2" value="Add">
                                                </div>						
                                            </div>
                                        </div>
                                    </div>					
                                </form>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- successfully add popup -->
<div class="position-fixed d-none" id="successfully-add">
    <div class="display_table">
        <div class="display_table_cell text-center">
            <div class="width_500 display_inilin_block text-center ">
                <div class="form_section mb-0">
                    <div class="col-12">
                        <div class="row justify-content-md-center">
                            <div class="col-12 pt-0 pb-2 text-right">
                                <a href="javascript:void(0);" class="close-button" data-action='add-customer'><img src="images/icons/cross.png"></a>
                            </div>
                            <div class="col-10 mb-5">
                                <img src="images/icons/tick-green.png" class="mb-5">
                                <h2 class="verify-email-text">Email notification sent to the customer's email account, the customer has to verify the email.</h2>
                            </div>
                            <div class="col-12">
                                <a href="javascript:void(0);" data-action="successfully-add"  class="close-button btn-save btn mr-2">OK</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection