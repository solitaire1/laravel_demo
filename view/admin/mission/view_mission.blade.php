@extends('layouts.index')
@section('content')
<?php

use App\User; ?> 
<div class="col-10 col-sm-8 col-md-9 col-lg-10 col-xl-10 col-lg content_side">
    <div class="row">
        <header>
            <div class="col">
                <h2>
                    <span>Missions / </span> <a href="javascript:void(0);">Create a Mission</a> <a href="{{url('/logout')}}" class="float-right">Log out <img src="{{ app('url')->asset('public/'.'images/icons/icon-logout.png') }}"></a>
                </h2>
            </div>
        </header>
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <section class="data_section">
            <div class="col">
                <div class="row">
                    <div class="col-12"
                         <div class="form_section">
                            <fieldset>
                                <legend>Mission Detail</legend>
                                <div class="row justify-content-md-center" >
                                    <div class="col-12 col-sm-12">
                                        <form class="row" action="" method="post">

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group row">
                                                    <label class="col-12 col-lg-4 col-form-label">Customer Name</label>
                                                    <div class="col-lg-8 col-12">
                                                        <span class="form-control">{{ User::getUserByid($mission->customer_id) }}</span>
                                                    </div>	
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Service Provider</label>
                                                    <div class="col-lg-8 col-12">
                                                        <span class="form-control">{{ User::getUserByid($mission->service_provider_id) }}</span>
                                                    </div>

                                                </div>	
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Object Leader</label>
                                                    <div class="col-lg-8 col-12 ">
                                                        <span class="form-control">{{ User::getUserByid($mission->object_leader_id) }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Address</label>
                                                    <div class="col-lg ">
                                                        <span class="form-control">{{$mission->address}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Mission Details</label>
                                                    <div class="col-lg ">
                                                        <span class="form-control">{{$mission->detail}}</span>
                                                    </div>
                                                </div>	
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Unique Code</label>
                                                    <div class="col-lg ">
                                                        <span class="form-control">{{$mission->unique_code}}</span>
                                                    </div>
                                                </div>	
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                        <?php
                        if (count($dispatchMission) > 0) {

                            $machinery_detail = json_decode($dispatchMission->machinery_detail, true);
                            $chemical_detail = json_decode($dispatchMission->chemical_detail, true);
                            ?>
                            <div class="form_section">
                                <fieldset>
                                    <legend>Dispatch Details</legend>
                                    <div class="row justify-content-md-center" >
                                        <div class="col-12 col-sm-12">
                                            <form class="row" action="" method="post">

                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group row">
                                                        <label class="col-12 col-lg-4 col-form-label">Detail</label>
                                                        <div class="col-lg-8 col-12">
                                                            <span class="form-control">
                                                                @if($dispatchMission->dispatch_detail)
                                                                {{ $dispatchMission->dispatch_detail }}
                                                                @else
                                                                {{'N/A'}}
                                                                @endif
                                                            </span>
                                                        </div>	
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group row">
                                                        <label class="col-lg-4 col-form-label">Manpower Detail</label>
                                                        <div class="col-lg-8 col-12 ">
                                                            <span class="form-control">
                                                                @if($dispatchMission->manpower_detail)
                                                                {{ $dispatchMission->manpower_detail }}
                                                                @else
                                                                {{'N/A'}}
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group row">
                                                        <label class="col-lg-4 col-form-label">Accept Time</label>
                                                        <div class="col-lg ">
                                                            <span class="form-control">
                                                                @if($dispatchMission->accept_time)
                                                                {{ $dispatchMission->accept_time }}
                                                                @else
                                                                {{'N/A'}}
                                                                @endif</span>
                                                        </div>
                                                    </div>	
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group row">
                                                        <label class="col-lg-4 col-form-label">Temperature</label>
                                                        <div class="col-lg-8 col-12">
                                                            <span class="form-control">
                                                                @if($dispatchMission->temperature)
                                                                {{ $dispatchMission->temperature." &#8451;" }}
                                                                @else
                                                                {{'N/A'}}
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>	
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group row">
                                                        <label class="col-lg-4 col-form-label">Chemical detail</label>
                                                        <div class="col-lg-8 col-12">
                                                            <ul class="form-control not-list-style"> <?php
                                                                if (count($chemical_detail) > 0) {
                                                                    foreach ($chemical_detail as $Key => $Cval) {
                                                                        ?>
                                                                        <li><b><?php echo "Chemical Name : " . $Cval['itemName']; ?></b></li>
                                                                        <li><?php echo "Quantity : " . $Cval['quantity'] . " Kg"; ?></li> 
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <li><?php echo "N/A"; ?></li>
                                                                    <?php
                                                                    }
                                                                    ?></ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group row">
                                                        <label class="col-lg-4 col-form-label">Machinery Detail</label>
                                                        <div class="col-lg-8 col-12">
                                                            <ul class="form-control not-list-style"> 
                                                                <?php
                                                                if (count($machinery_detail) > 0) {
                                                                    foreach ($machinery_detail as $Key => $val) {
                                                                        ?>
                                                                        <li><b><?php echo "Machine Name : " . $val['itemName']; ?></b></li>
                                                                        <li><?php echo "Quantity : " . $val['quantity']; ?></li>
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <li><?php echo "N/A"; ?></li>
                                                                    <?php
                                                                    }
                                                                    ?></ul>
                                                        </div>

                                                    </div>	
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <?php
                        } if (count($finalMissionDetail) > 0) {
                            $machinery_detailFinal = json_decode($finalMissionDetail->machinery_detail, true);
                            $chemical_detailFinal = json_decode($finalMissionDetail->chemical_detail, true);
                            ?>

                            <div class="form_section">
                                <fieldset>
                                    <legend>Final Mission Details</legend>
                                    <div class="row justify-content-md-center" >
                                        <div class="col-12 col-sm-12">
                                            <form class="row" action="" method="post">

                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group row">
                                                        <label class="col-12 col-lg-4 col-form-label">Machinery Detail</label>
                                                        <div class="col-lg-8 col-12">
                                                            <ul class="form-control not-list-style"> <?php
                                                                if (count($machinery_detailFinal) > 0) {
                                                                    foreach ($machinery_detailFinal as $Key => $CfMval) {
                                                                        ?>
                                                                        <li><b><?php echo "Machine Name : " . $CfMval['itemName']; ?></b></li>
                                                                        <li><?php echo "Quantity : " . $CfMval['quantity']; ?></li> 

                                                                        <?php
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <li><?php echo "N/A"; ?></li>
                                                                    <?php
                                                                    }
                                                                    ?></ul>
                                                        </div>	
                                                    </div>
                                                </div>

                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group row">
                                                        <label class="col-lg-4 col-form-label">Manpower Detail</label>
                                                        <div class="col-lg-8 col-12">
                                                            <span class="form-control">
                                                                @if($finalMissionDetail->manpower_detail)
                                                                {{ $finalMissionDetail->manpower_detail }}
                                                                @else
                                                                {{'N/A'}}
                                                                @endif
                                                            </span>
                                                        </div>

                                                    </div>	
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group row">
                                                        <label class="col-lg-4 col-form-label">Chemical Detail</label>
                                                        <div class="col-lg-8 col-12 ">
                                                            <ul class="form-control not-list-style">
                                                                <?php
                                                                if (count($chemical_detailFinal) > 0) {
                                                                    foreach ($chemical_detailFinal as $KeyF => $CFval) {
                                                                        ?>
                                                                        <li><b><?php echo "Chemical Name : " . $CFval['itemName']; ?></b></li>
                                                                        <li><?php echo "Quantity : " . $CFval['quantity'] . " Kg<br>"; ?></b></li>
                                                                       <?php }
                                                                        } else {
                                                                        echo "N/A";
                                                                        }
                                                                        ?>
                                                            </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection